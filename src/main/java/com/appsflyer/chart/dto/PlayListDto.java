package com.appsflyer.chart.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by alexander on 02/09/2017.
 */
public class PlayListDto implements Serializable {

    private static final long serialVersionUID = 8280424090481213445L;
    private String usrId;
    private Date date;

    public PlayListDto() {
    }

    public PlayListDto(String usrId, Date date) {
        this.usrId = usrId;
        this.date = date;
    }

    public String getUsrId() {
        return usrId;
    }

    public void setUsrId(String usrId) {
        this.usrId = usrId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PlayListDto that = (PlayListDto) o;

        return new EqualsBuilder()
                .append(usrId, that.usrId)
                .append(date, that.date)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(usrId)
                .append(date)
                .toHashCode();
    }
}
