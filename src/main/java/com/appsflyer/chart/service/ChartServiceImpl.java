package com.appsflyer.chart.service;

import com.appsflyer.chart.dto.PlayListDto;
import javafx.util.Pair;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by alexander on 04/09/2017.
 */
@Service
public class ChartServiceImpl implements ChartService, Serializable {

    private static final long serialVersionUID = 6251517808733466842L;

    @Autowired
    private SparkConf sparkConf;

    @Value("${chart.storage.path}")
    private String storageFilePath;

    @Value("${chart.chart-size}")
    private int chartSize;

    @Value("${chart.amount-sessions}")
    private int amountSessions;

    @Value("${chart.session-iterator}")
    private int sessionIterator;

    private JavaRDD<String> rdd;

    @Override
    public Integer getUsersCount() {
        return getUsers().size();
    }

    @Override
    public Integer getUsersTransformedCount() {
        return transformedUsers().size();
    }

    /**
     * this method is return list of PlayListDto (it contains userId and date)
     * approximate tame execution 1 min on my local pc with 6gb of ram
     * NOTE: this method needs to make cacheable
     *
     * @return List<PlayListDto>
     * @see PlayListDto
     */

    //@Cacheable("users")
    private List<PlayListDto> getUsers() {
        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

        rdd = javaSparkContext.textFile(storageFilePath, 1);

        return rdd.map(new Function<String, PlayListDto>() {
            @Override
            public PlayListDto call(String s) throws Exception {
                String[] usr = s.split("\\t");
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date d = dateFormat.parse(usr[1]);
                return new PlayListDto(usr[0], d);
            }
        }).collect();
    }

    /**
     * methods transforms List<PlayListDto> to map Map<String, ArrayList<Date>> where String is userId and ArrayList<Date> songs date
     * approximate tame execution 10 min on my local pc with 6gb of ram
     *
     * @return Map<String, ArrayList<Date>>
     */
    private Map<String, List<Date>> transformedUsers() {
        List<PlayListDto> listDtos = getUsers();
        Map<String, List<Date>> documentMap = new HashMap<>();
        listDtos.forEach(playListDto -> {
            String usrId = playListDto.getUsrId();
            Date date = playListDto.getDate();
            List<Date> documentMapDates = documentMap.get(usrId);
            if (documentMapDates == null) {
                List<Date> dates = new ArrayList<>();
                dates.add(date);
                documentMap.put(usrId, dates);
            } else {
                documentMapDates.add(date);
            }
        });
        return documentMap;
    }

    /**
     * this methods iterates over all elements from transformedUsers and calculate longest possible userSessions
     * approximate tame execution 20 min on my local pc with 6gb of ram
     * Note: this calculation can be performed using spark api, however since i have not much time it will be done using java
     *
     * @return List<Integer>
     */
    @Override
    public Map<List<Date>, Integer> getLongestPossibleSessions() {

        Map<String, List<Date>> usersMap = transformedUsers();

        Map<List<Date>, Integer> userSessions = new HashMap<>();

        usersMap.forEach((userId, arrayListBiConsumer) -> {
            final int[] sessionLength = {1};
            arrayListBiConsumer.stream().parallel().forEach(date -> {
                Date nextDate = DateUtils.addMinutes(date, sessionIterator);
                if (arrayListBiConsumer.contains(nextDate)) {
                    sessionLength[0]++;
                    userSessions.put(arrayListBiConsumer, sessionLength[0]);
                }
            });
        });
        return userSessions;
    }

    /**
     * Method calculates 50 loges actual transaction
     *
     * @return ArrayList<ArrayList Date>
     * <p>
     * approximate time execution 20 min
     */
    public List<List<Date>> get50LongestSessions() {
        Map<List<Date>, Integer> longestSessions = getLongestPossibleSessions();

        List<List<Date>> actualSessions = new ArrayList<>();
        longestSessions.entrySet().parallelStream().forEach(arrayListIntegerEntry -> {
            Integer numberOfSessions = arrayListIntegerEntry.getValue();
            List<Date> userDates = arrayListIntegerEntry.getKey();

            List<Date> actualSession = arrayListIntegerEntry.getKey();

            userDates.sort(Date::compareTo);
            Date startDate = userDates.get(0);
            actualSession.add(startDate);

            Stream<Integer> numberOfSessionsStream = Stream.iterate(sessionIterator, n -> n + sessionIterator)
                    .limit(numberOfSessions);


            numberOfSessionsStream.parallel().forEach(i -> {
                Date nextDate = DateUtils.addMinutes(startDate, i);
                if (userDates.contains(nextDate)) {
                    actualSession.add(nextDate);
                }
            });

            actualSessions.add(actualSession);
        });

        Comparator<List> lengthComparator = new Comparator<List>() {
            @Override
            public int compare(List o1, List o2) {
                return Integer.compare(o1.size(), o2.size());
            }
        };

        actualSessions.sort(lengthComparator);
        return actualSessions.subList(0, amountSessions);
    }

    /**
     * This methods return chart of top 10 songs from given transaction list
     * approximate time execution 30 min
     * @return List<Map.Entry<String, Long>> (String - song name , Long number of counts)
     */
    public List<Map.Entry<String, Long>> getTopSongs() {
        List<String> songs = new ArrayList<>();
        List<Date> longestSessions = get50LongestSessions().stream().parallel().flatMap(List::stream).collect(Collectors.toList());


        List<Pair<Date, String>> collect = rdd.map(new Function<String, Pair<Date, String>>() {
            @Override
            public Pair<Date, String> call(String s) throws Exception {
                String[] usr = s.split("\\t");
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date d = dateFormat.parse(usr[1]);
                return new Pair(d, usr[5]);
            }
        }).collect();

        collect.parallelStream().forEach(dateStringPair -> {
            Date songDate = dateStringPair.getKey();
            if (longestSessions.contains(songDate)) {
                songs.add(dateStringPair.getValue());
            }
        });

        Map<String, Long> valueCounts =
                songs.stream().collect(Collectors.groupingBy(o -> o, Collectors.counting()));

        Stream<Map.Entry<String, Long>> sorted =
                valueCounts.entrySet().stream().parallel()
                        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));

        Stream<Map.Entry<String, Long>> limit = sorted.limit(chartSize);
        return limit.collect(Collectors.toList());
    }
}
