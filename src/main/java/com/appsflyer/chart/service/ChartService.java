package com.appsflyer.chart.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by alexander on 04/09/2017.
 */
public interface ChartService {

    /**
     * method returns count of parsed Users
     * */
    Integer getUsersCount();
    Integer getUsersTransformedCount();
    Map<List<Date>, Integer> getLongestPossibleSessions();
    List<List<Date>> get50LongestSessions();
    List<Map.Entry<String, Long>> getTopSongs();
}
