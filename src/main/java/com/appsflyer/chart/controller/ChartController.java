package com.appsflyer.chart.controller;

import com.appsflyer.chart.service.ChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by alexander on 04/09/2017.
 */

@RestController
public class ChartController {

    @Autowired
    ChartService service;

    @GetMapping("/usersCount")
    public Integer usersCount() {
        return service.getUsersCount();
    }

    @GetMapping("/usersTransformedCount")
    public Integer usersTransformedCount() {
        return service.getUsersTransformedCount();
    }

    @GetMapping("/actualSessions")
    public List<List<Date>> getLongestPossibleSessions() {
        return service.get50LongestSessions();
    }

    @GetMapping("/top")
    public List<Map.Entry<String, Long>> findSongs() {
        return service.getTopSongs();
    }
}
