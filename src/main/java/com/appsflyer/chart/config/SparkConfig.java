package com.appsflyer.chart.config;

import org.apache.spark.SparkConf;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by alexander on 04/09/2017.
 */
@Configuration
public class SparkConfig {

    @Bean
    public SparkConf config(){
        SparkConf sparkConf = new SparkConf();
        sparkConf.setAppName("Chart");
        sparkConf.set("spark.driver.allowMultipleContexts", "true");
        sparkConf.setMaster("local");
        return sparkConf;
    }
}
