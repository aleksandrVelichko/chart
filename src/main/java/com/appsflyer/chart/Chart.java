package com.appsflyer.chart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chart {

    public static void main(String[] args) {
        SpringApplication.run(Chart.class, args);
    }

}