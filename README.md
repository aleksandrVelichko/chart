# Synopsis
The Chart project.

## TODO
There are few things that needs to be done. since i was having only 2 days to complete this task
##### add cache
##### add unit tests 
##### add swagger   

## Overview
This Service is calculating top 10 most popular songs from datasource tsv file

## Contact
Aleksandr Velichko mrdezzdemon@gmail.com

## Endpoints
#####  /health - heartbeat check
#####  /usersCount - get count of actual users 
#####  /usersTransformedCount get count of actual users transformed for data processing
#####  /actualSessions - get list of constructed user sessions from data source
#####  /top get chart of top 10 songs 
  
## Building
from signature:
    `mvn clean install`

##Configuration
all configuration information stored in application.yml file (/my-app/src/main/resources/application.yml)
 `chart.storage.path = path to datasource file`

## Run project 
from pom directory with default settings (application.yml)
    `mvn spring-boot:run -Drun.jvmArguments="-Xmx4024m -Xms2024m -XX:-UseGCOverheadLimit" -Drun.profiles=dev`



